var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");
const fs = require("fs");

const uploadExcel = (fileName, file, res) => {
  try {
    if (!fs.existsSync("./Backup")) {
      fs.mkdirSync("./Backup");
    }
  } catch (err) {
    console.error(err);
  }
  let exceltojson;
  if (fileName.split(".")[fileName.split(".").length - 1] === "xlsx") {
    exceltojson = xlsxtojson;
  } else {
    exceltojson = xlstojson;
  }
  file.mv("./Backup/" + fileName, (err) => {
    if (err) {
      console.log(err);
    } else {
      try {
        exceltojson(
          {
            input: "./Backup/" + fileName,
            output: null,
            lowerCaseHeaders: false,
          },
          async (err, result) => {
            if (err) {
              return res.json({ error_code: 1, err_desc: err, data: null });
            }
            res.json(result);
          }
        );
      } catch (e) {
        res.alert("file uploaded");
      }
      const deleteFile = "./Backup/" + fileName;
      fs.unlink(deleteFile, function (err) {
        if (err) {
          throw err;
        }
      });
    }
  });
};

module.exports = uploadExcel;
