const aws = require("aws-sdk/clients/s3");
const fs = require("fs");
const crypto = require("crypto");
const { promisify } = require("util");
const { randomBytes } = require("crypto");
const bucketName = process.env.BUCKET_NAME;
const accessKeyId = process.env.ACCESS_KEY;
const secretAccessKey = process.env.SECRET_KEY;
const region = process.env.REGION;

//set up s3

const s3 = new aws({
  accessKeyId,
  secretAccessKey,
  region,
  signatureVersion: "v4",
});

//uploadFile with key
const uploadFileByKey = (file, key) => {
  const fileStream = fs.createReadStream(file.path);
  const uploadParams = {
    Bucket: bucketName,
    Body: fileStream,
    Key: key,
  };
  return s3.upload(uploadParams).promise();
};

//upload a file to s3
const uploadFile = (file) => {
  const fileStream = fs.createReadStream(file.path);
  const uploadParams = {
    Bucket: bucketName,
    Body: fileStream,
    Key: file.filename,
  };
  return s3.upload(uploadParams).promise();
};

//upload a file to s3
const uploadSignature = async (file) => {
  const fileName = "uploaded_on_" + Date.now() + ".jpg";

  const uploadParams = {
    Bucket: bucketName,
    Body: file,
    Key: fileName,
  };
  const res = s3.upload(uploadParams, (error, data) => {
    console.log(error, data);
    if (error) {
      return res.json({
        status: "error",
        error: error.message || "Something went wrong",
      });
    }

    // const params = {
    //   Bucket: "prime-technical-services-bucket",
    //   Key: fileName,
    //   Expires: 60,
    // };

    // const signedURL = s3.getSignedUrl("getObject", params);

    return data;
  });
};

//get File
const getFileStream = (fileKey) => {
  const downloadParams = {
    Key: fileKey,
    Bucket: bucketName,
  };
  return s3.getObject(downloadParams).createReadStream();
};

//delete File
const deleteFile = (fileKey) => {
  const bucketParams = {
    Key: fileKey,
    Bucket: bucketName,
  };
  return s3.deleteObject(bucketParams).promise();
};

//get Url
const generateUploadUrl = async () => {
  const rawBytes = await randomBytes(16);
  const imageName = rawBytes.toString("hex");
  const params = {
    Bucket: bucketName,
    Key: imageName,
    Expires: 60,
  };
  const uploadUrl = await s3.getSignedUrlPromise("putObject", params);
  const readUrl = await s3.getSignedUrlPromise("getObject", params);
  return { readUrl, uploadUrl };
};

module.exports = {
  generateUploadUrl,
  getFileStream,
  uploadFile,
  deleteFile,
  uploadSignature,
  uploadFileByKey,
};
