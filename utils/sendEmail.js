const nodemailer = require("nodemailer");
const { google } = require("googleapis");
const CLIENT_ID =
  "817772987879-9npgeqe6nrt6bpvogs40p9cb38am4i5i.apps.googleusercontent.com";
const CLIENT_SECRET = "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";
const REDIRECT_URI = "https://developers.google.com/oauthplayground";
const REFRESH_TOKEN =
  "1//04fS3wYHCczpmCgYIARAAGAQSNwF-L9Irwa7esEEedJmQoUtZsFm1rnTD6FbQqyoxFK6usENYZIjReecE8bygGEg3-rLzdQbLC1Q";

const oAuth2Client = new google.auth.OAuth2(
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URI
);
oAuth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

const sendEmail = async (email, subject, body, EmailAttachMents) => {
  try {
    const accessToken = await oAuth2Client.getAccessToken();
    const transport = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user: "sij12340000@gmail.com",
        clientId: CLIENT_ID,
        clientSecret: CLIENT_SECRET,
        refreshToken: REFRESH_TOKEN,
        accessToken: accessToken,
      },
      tls: {
        rejectUnauthorized: false,
      },
    });
    let mainOptions;
    if (EmailAttachMents) {
      mainOptions = {
        from: "<sij12340000@gmail.com>",
        to: email,
        subject: subject,
        text: body,
        attachments: [
          {
            path: EmailAttachMents,
          },
        ],
      };
    } else {
      mainOptions = {
        from: "<sij12340000@gmail.com>",
        to: email,
        subject: subject,
        text: body,
      };
    }

    const result = await transport.sendMail(mainOptions);
    return result;
  } catch (error) {
    return error;
  }
};
module.exports = sendEmail;
