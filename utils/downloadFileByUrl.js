const fs = require("fs");
const fetch = require("cross-fetch");

const downloadFileByUrl = async (fileUrl,  name, pathName) => {
  const res = await fetch(fileUrl);
  let data = await res.arrayBuffer(); // you image stored on arrayBuffer variable;
  data = Buffer.from(data);
  if(pathName){
    fs.writeFileSync(`./Backup/${pathName}/${name}`, data, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }
  else{
    fs.writeFileSync(`./Backup/${name}`, data, (err) => {
      if (err) {
        console.log(err);
      }
    });

  }
 
};

module.exports = downloadFileByUrl;
