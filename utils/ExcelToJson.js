var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");
const fs = require("fs");
const CheckList = require("../models/CheckList");

const ExcelToJson = (fileName, file, res) => {
  try {
    if (!fs.existsSync("./Backup")) {
      fs.mkdirSync("./Backup");
    }
  } catch (err) {
    console.error(err);
  }
  let exceltojson;
  if (fileName.split(".")[fileName.split(".").length - 1] === "xlsx") {
    exceltojson = xlsxtojson;
  } else {
    exceltojson = xlstojson;
  }
  file.mv("./Backup/" + fileName, (err) => {
    if (err) {
      console.log(err);
    } else {
      try {
        exceltojson(
          {
            input: "./Backup/" + fileName,
            output: null,
            lowerCaseHeaders: false,
          },
          async (err, result) => {
            if (err) {
              return res.json({ error_code: 1, err_desc: err, data: null });
            }

            const addChecklist = [
              {
                Field_Text: "DEFAULT DETAILS",
                Field_Type: "section",
                Radio_Values: "",
                Default_Value: "",
                Field_Name: "",
                Field_Settings: "",
                Tips: "",
              },
              {
                Field_Text: "Checklist Subtype",
                Field_Type: "text",
                Radio_Values: "",
                Default_Value: "",
                Field_Name: "checklistSubtype",
                Field_Settings: "",
                Tips: "",
              },
              {
                Field_Text: "NAME AND ADDRESS OF THE OWNER",
                Field_Type: "client",
                Radio_Values: "",
                Default_Value: "",
                Field_Name: "client",
                Field_Settings: "all_caps",
                Tips: "",
              },
              // {
              //   Field_Text: "ID / PLANT / OWNER / FLEET No.",
              //   Field_Type: "text",
              //   Radio_Values: "",
              //   Default_Value: "",
              //   Field_Name: "identificationNo",
              //   Field_Settings: "no_space; all_caps",
              //   Tips: "",
              // },
              {
                Field_Text: "PLACE OF INSPECTION",
                Field_Type: "inspectionLocation",
                Radio_Values: "",
                Default_Value: "",
                Field_Name: "inspectionLocation",
                Field_Settings: "",
                Tips: "",
              },

              {
                Field_Text: "DATE OF INSPECTION",
                Field_Type: "date",
                Radio_Values: "",
                Default_Value: "",
                Field_Name: "dateOfInspection",
                Field_Settings: "",
                Tips: "",
              },

              {
                Field_Text: "Sticker No.",
                Field_Type: "default",
                Radio_Values: "",
                Default_Value: "",
                Field_Name: "stickerNo",
                Field_Settings: "",
                Tips: "",
              },

              // {
              //   Field_Text: "Registration No.",
              //   Field_Type: "text",
              //   Radio_Values: "",
              //   Default_Value: "",
              //   Field_Name: "registrationNo",
              //   Field_Settings: "no_space; all_caps",
              //   Tips: "",
              // },

              // {
              //   Field_Text: "Model No",
              //   Field_Type: "text",
              //   Radio_Values: "",
              //   Default_Value: "",
              //   Field_Name: "model",
              //   Field_Settings: "no_space; all_caps",
              //   Tips: "",
              // },
              // {
              //   Field_Text: "Serial Number",
              //   Field_Type: "text",
              //   Radio_Values: "",
              //   Default_Value: "",
              //   Field_Name: "serialNumber",
              //   Field_Settings: "no_space; all_caps",
              //   Tips: "",
              // },
            ];
            const addMoreChecklist = [
              {
                Field_Text: "Add Inspection Pictures",
                Field_Type: "button",
                Radio_Values: "",
                Default_Value: "",
                Field_Name: "pictures",
                Field_Settings: "",
                Tips: "",
              },

              {
                Field_Text: "Result",
                Field_Type: "radio",
                Radio_Values: "Pass ; Fail",
                Default_Value: "Pass",
                Field_Name: "result",
                Field_Settings: "",
                Tips: "",
              },
              // {
              //   Field_Text: "Date of Next Inspection",
              //   Field_Type: "nextYear",
              //   Radio_Values: "",
              //   Default_Value: "",
              //   Field_Name: "dateOfNextInspection",
              //   Field_Settings: "all_caps",
              //   Tips: "",
              // },
              // {
              //   Field_Text: "Inspected By",
              //   Field_Type: "inspectedBy",
              //   Radio_Values: "",
              //   Default_Value: "",
              //   Field_Name: "inspectedBy",
              //   Field_Settings: "",
              //   Tips: "",
              // },
              // {
              //   Field_Text: "Approved By",
              //   Field_Type: "approvedBy",
              //   Radio_Values: "",
              //   Default_Value: "",
              //   Field_Name: "approvedBy",
              //   Field_Settings: "",
              //   Tips: "",
              // },
          
              {
                Field_Text: "certificate No",
                Field_Type: "default",
                Radio_Values: "",
                Default_Value: "",
                Field_Name: "certificateNo",
                Field_Settings: "",
                Tips: "",
              },
              {
                Field_Text: "Order No",
                Field_Type: "default",
                Radio_Values: "",
                Default_Value: "",
                Field_Name: "orderNo",
                Field_Settings: "",
                Tips: "",
              },
            ];
            const newChecklistData = [
              ...addChecklist,
              ...addMoreChecklist,
              ...result,
            ];
            const checkList = {
              checkList: newChecklistData,
              name: fileName.split(".")[0],
              date: new Date(),
              status: false,
            };

            const newChecklist = new CheckList(checkList);
            const data = await newChecklist.save();

            if (true) {
              const documents = await CheckList.find();
              res.json(documents);
            }
          }
        );
      } catch (e) {
        res.alert("file uploaded");
      }
      const deleteFile = "./Backup/" + fileName;
      fs.unlink(deleteFile, function (err) {
        if (err) {
          throw err;
        }
      });
    }
  });
};

module.exports = ExcelToJson;
