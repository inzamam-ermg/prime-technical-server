const fs = require("fs");
const fetch = require("cross-fetch");

const picturesByUrl = async (pictures,pathName,extension) => {
  for (let i = 0; i < pictures.length;  i++) {
    const url = pictures[i];
    const res = await fetch(url);
    let data = await res.arrayBuffer(); // you image stored on arrayBuffer variable;
    data = Buffer.from(data);
    fs.writeFileSync(`./Backup/${pathName}/${i}${extension}`, data, (err) => {
      if (err) {
        console.log(err);
      } 
    });
    if(i === pictures.length-1){
      return true
    }
    
  }


};

module.exports = picturesByUrl;
