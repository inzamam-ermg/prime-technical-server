const { PDFDocument } = require("pdf-lib");
const fetch = require("cross-fetch");

const uploadPdf = async (url) => {
  const res = await fetch(url);
  const formPdfBytes = await res.arrayBuffer();

  // Load a PDF with form fields
  const pdfDoc = await PDFDocument.load(formPdfBytes);

  // Get the form containing all the fields
  const form = pdfDoc.getForm();
  const fields = form.getFields();

  const formFields = [];
  fields.forEach((field) => {
    const type = field.constructor.name;
    const name = field.getName();
    const data = { type, name };
    formFields.push(data);
  });

  return formFields;
};

module.exports = uploadPdf;
