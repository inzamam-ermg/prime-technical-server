const fs = require("fs");
const xlsx = require("xlsx");

const slice = (array, dataPerPage) => {
  let newArray = [];

  for (
    let start = 1;
    start <= Math.floor(array.length / dataPerPage);
    start++
  ) {
    let lastLog = start * dataPerPage;
    let firstLog = lastLog - dataPerPage;

    newArray.push(array.slice(firstLog, lastLog));
  }
  const index = Math.floor(array.length / dataPerPage);
  const lastArrayStartIndex = index * dataPerPage;
  let lastArray = array.slice(lastArrayStartIndex, array.length);
  newArray.push(lastArray);
  return newArray;
};

const createExcelSheet =async (fileName, data, dataPerPage,pathName) => {
  const workbook = xlsx.utils.book_new();
  const pages = slice(data, dataPerPage);
  pages.map((singlePage) => {
    let worksheet = xlsx.utils.json_to_sheet(singlePage);
    xlsx.utils.book_append_sheet(
      workbook,
      worksheet,
      `${fileName}${pages.indexOf(singlePage)}`
    );
  });
  let newPath;
  xlsx.writeFile(workbook, `${fileName}.xlsx`);
  var oldPath = `./${fileName}.xlsx`;
  if(pathName){
    newPath=`./Backup/${pathName}/${fileName}.xlsx`;
  }
  else{
    newPath = `./Backup/${fileName}.xlsx`;
  }
 

  fs.rename(oldPath, newPath, function (err) {
    if (err) throw err;
  });
  return true;
};

module.exports = createExcelSheet;
