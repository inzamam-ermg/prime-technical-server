const fs = require("fs");
const fetch = require("cross-fetch");

const certificatesByUrl = async (pdf,pathName) => {
  for (let i = 0; i < pdf.length;  i++) {
    const ele = pdf[i]
    const name = ele.name;
    const url = ele.value
    const res = await fetch(url);
    let data = await res.arrayBuffer(); // you image stored on arrayBuffer variable;
    data = Buffer.from(data);
    fs.writeFileSync(`./Backup/${pathName}/${name}`, data, (err) => {
      if (err) {
        console.log(err);
      } 
    });
    if(i === pdf.length-1){
      return true
    }
    
  }


};

module.exports = certificatesByUrl;
