// external imports
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");
require("dotenv").config();
const http = require("http");
const path = require("path");
const fs = require("fs");

// internal imports
const userRouter = require("./router/userRouter");
const deviceAuthRouter = require("./router/deviceAuthRouter");
const certificateRouter = require("./router/certificateRouter");
const stickerRouter = require("./router/stickerRouter");
const checkListRouter = require("./router/checkListRouter");
const backupRouter = require("./router/backupRouter");
const templateMappingRouter = require("./router/templateMappingRouter");
const jobRouter = require("./router/jobRouter");
const bucketRouter = require("./router/bucketRouter");
const resumeJobRouter = require("./router/ResumeJobRouter");
const rasterizerRouter = require("./router/rasterizerRouter");
const signRouter = require("./router/signRouter");
const mailRouter = require("./router/mailRouter");
const companyDetailsRouter = require("./router/companyDetailsRouter");
const FooterRouter = require("./router/FooterRouter");
const notificationRouter = require("./router/notificationRouter");
const bannerRouter = require("./router/bannerRouter");
const readPDF = require("./utils/readPDF");

const Log = require("./models/Log");
const { getFileStream } = require("./utils/s3Bucket");

const app = express();

// database connection
const uri =
  "mongodb+srv://freelancing:freelancing123@cluster0.g95ef.mongodb.net/PrimeTechnical?retryWrites=true&w=majority";

const connectDb = async () => {
  try {
    const conn = await mongoose.connect(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log("database connection successful!");
  } catch (error) {
    console.log(error);
  }
};

const port = process.env.PORT || 5050;

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin,X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET,POST,PATCH,PUT,DELETE,OPTIONS"
  );
  next();
});
app.use(cors());
app.use(
  bodyParser.json({
    limit: "50MB",
  })
);

app.use(
  bodyParser.urlencoded({
    limit: "50MB",
    parameterLimit: 100000,
    extended: true,
  })
);
app.use(express.json());
app.use(express.static(path.join(__dirname + "uploadedPdf")));
app.use("/rasterizedPdf", express.static("rasterizedPdf"));
app.use("/font", express.static("fonts"));

//routing setup
app.use("/users", userRouter);
app.use("/device_auth", deviceAuthRouter);
app.use("/certificates", certificateRouter);
app.use("/sticker", stickerRouter);
app.use("/checkList", checkListRouter);
app.use("/backup", backupRouter);
app.use("/template", templateMappingRouter);
app.use("/job", jobRouter);
app.use("/s3Bucket", bucketRouter);
app.use("/sendMail", mailRouter);
app.use("/resumeJob", resumeJobRouter);
app.use("/footer", FooterRouter);
app.use("/rasterize", rasterizerRouter);
app.use("/sign", signRouter);
app.use("/banner", bannerRouter);
app.use("/notification", notificationRouter);

app.use("/saveCompanyData", companyDetailsRouter);

app.post("/logs", async (req, res) => {
  console.log(req.body.data);
  try {
    const newLog = new Log(req.body.data);
    const ack = await newLog.save();
    res.json(ack);
  } catch (err) {
    res.send({ err: "Unknown error" });
  }
});

app.get("/logs", async (req, res) => {
  try {
    const logs = await Log.find();
    res.json(logs);
  } catch (err) {
    res.send({ err: "Unknown error" });
  }
});

app.post("/readpdf", async (req, res) => {
  console.log(req.body);
  const fields = await readPDF(req.body.url);
  res.send(fields);
});

// get files from s3 bucket

app.get("/:key", (req, res) => {
  try {
    const key = req.params.key;
    const readStream = getFileStream(key);
    readStream
      .on("error", (e) => {
        res.json("not found");
      })
      .pipe(res)
      .on("data", (data) => {
        console.log(data);
      });
  } catch (error) {
    res.json({ err: "failed to get" });
  }
});

app.get("/font/:name", async (req, res) => {
  console.log(req.params.name);
  try {
    res.download(`fonts/${req.params.name}`);
  } catch (err) {
    res.send({ err: "Unknown error" });
  }
});

const getCollectionSize = (collection) => {
  collection
    .stats()
    .then((count_documents) => {
      const size = count_documents.totalSize / 1024;
      const mb = size / 1024;
      if (size > 1) {
        console.log("download");
      }
    })
    .catch((err) => {
      console.log(err.Message);
    });
};

app.get("/", (req, res) => {
  res.send("Hi, I'm healthy!!!");
});

connectDb().then(() => {
  app.listen(port, () => {
    console.log(`Server listening on post: ${port}`);
  });
});
