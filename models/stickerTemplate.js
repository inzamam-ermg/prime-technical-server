const mongoose = require("mongoose");

const stickerTemplateSchema = mongoose.Schema(
  {
    value: String,
    name: String,
    date: Date,
  },
  {
    timestamps: true,
  }
);

const StickerTemplate = mongoose.model(
  "StickerTemplate",
  stickerTemplateSchema
);

module.exports = StickerTemplate;
