const mongoose = require("mongoose");

const FooterSchema = mongoose.Schema(
  {
    name: String,
    value: String,
 
  },
  {
    timestamps: true,
  }
);

const Footer = mongoose.model("Footer", FooterSchema);

module.exports = Footer;
