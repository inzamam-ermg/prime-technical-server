const mongoose = require("mongoose");

const peopleSchema = mongoose.Schema(
  {
    name: String,
    email: String,
    role: Object,
    employeeId: String,
    signature: String,
    emailVerified: Boolean,
    isAdmin: Boolean,
    isSalesMan: Boolean,
    isSuperAdmin: Boolean,
    isInspector: Boolean,
    photoUrl: String,
    provider: String,
    uid: String,
    authorized: Object,
    status: Boolean,
  },
  {
    timestamps: true,
  }
);

const People = mongoose.model("People", peopleSchema);

module.exports = People;
