const mongoose = require("mongoose");

const jobSchema = mongoose.Schema(
  {
    companyName: String,
    client: String,
    clientEmail: String,
    jobNo: String,
    jobSerial: String,
    contactPerson: String,
    location: String,
    scheduleDate: String,
    comment: String,
    contactNo: String,
    whatsAppNo: String,
    inspectors: [String],
    AssignInspector: [String],
    salesMan: String,
    isInspectionComplete: Boolean,
    isComplete: Boolean,
    isEdit: Boolean,
    checklists: Array,
    isAuthorized: Boolean,
    quotations: Array,
    invoices: Array,
  },
  {
    timestamps: true,
  }
);

const Job = mongoose.model("Job", jobSchema);

module.exports = Job;
