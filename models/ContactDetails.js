const mongoose = require("mongoose");

const contactDetailsSchema = mongoose.Schema(
  {
    companyName: String,
    contactPerson: String,
    contactNo: String,
    client: String,
    clientEmail: String,
    contactId: String,
  },
  {
    timestamps: true,
  }
);

const ContactDetail = mongoose.model("ContactDetail", contactDetailsSchema);

module.exports = ContactDetail;
