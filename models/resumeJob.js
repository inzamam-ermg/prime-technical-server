const mongoose = require("mongoose");

const JobSchema = mongoose.Schema(
  {
    companyName: String,
    client: String,
    clientEmail: String,
    jobNo: String,
    jobSerial: String,
    contactPerson: String,
    location: String,
    scheduleDate: String,
    comment: String,
    jobId: String,
    contactNo: String,
    whatsAppNo: String,
    inspectors: [String],
    AssignInspector: [String],
    salesMan: String,
    isInspectionComplete: Boolean,
    isEdit: Boolean,
    checklists: Array,
    newChecklists: Array,
  },
  {
    timestamps: true,
  }
);

const resumeJob = mongoose.model("resumeJob", JobSchema);

module.exports = resumeJob;
