const mongoose = require("mongoose");

const certificateTemplateSchema = mongoose.Schema(
  {
    value: String,
    name: String,
    date: Date,
  },
  {
    timestamps: true,
  }
);

const CertificateTemplate = mongoose.model(
  "CertificateTemplate",
  certificateTemplateSchema
);

module.exports = CertificateTemplate;
