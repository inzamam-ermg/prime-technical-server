const mongoose = require("mongoose");

const bannerSchema = mongoose.Schema(
  {
    name: String,
    value: String,
 
  },
  {
    timestamps: true,
  }
);

const Banner = mongoose.model("banner", bannerSchema);

module.exports = Banner;
