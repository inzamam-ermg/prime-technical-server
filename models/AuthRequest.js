const mongoose = require("mongoose");

const authRequestSchema = mongoose.Schema(
  {
    userName: String,
    device: Object,
    userEmail: String,
    userName: String,
    comment: String,
  },
  {
    timestamps: true,
  }
);

const AuthRequest = mongoose.model("AuthRequest", authRequestSchema);

module.exports = AuthRequest;
