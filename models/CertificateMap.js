const mongoose = require("mongoose");

const certificateMapSchema = mongoose.Schema(
  {
    name: String,
    checklist: Object,
    certificateTemplate: Object,
    conclusion: String,
    date: Date,
    linkedField: Array,
  },
  {
    timestamps: true,
  }
);

const CertificateMap = mongoose.model("CertificateMap", certificateMapSchema);

module.exports = CertificateMap;
