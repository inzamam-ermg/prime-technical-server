const mongoose = require("mongoose");

const checkListSchema = mongoose.Schema(
  {
    name: String,
    checkList: Array,
    status: Boolean,
    date: Date,
  },
  {
    timestamps: true,
  }
);

const CheckList = mongoose.model("Checklist", checkListSchema);

module.exports = CheckList;
