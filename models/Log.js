const mongoose = require("mongoose");

const logSchema = mongoose.Schema(
  {
    name: String,
    message: String,
    date: Date,
    location: Object,
  },
  {
    timestamps: true,
  }
);

const Log = mongoose.model("Log", logSchema);

module.exports = Log;
