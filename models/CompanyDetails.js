const mongoose = require("mongoose");

const companyDetailsSchema = mongoose.Schema(
  {
    companyName: String,
    contactPerson: String,
    contactNo: String,
    client: String,
    clientEmail: String,
    companyId: String,
  },
  {
    timestamps: true,
  }
);

const CompanyDetail = mongoose.model("CompanyDetail", companyDetailsSchema);

module.exports = CompanyDetail;
