const mongoose = require("mongoose");

const NotificationSchema = mongoose.Schema(
  {
    name: String,
    userId:String,
    seenStatus:Array,
    message: String,
    type:String,
    date: Date,
  },
  {
    timestamps: true,
  }
);

const Notification = mongoose.model("Notification", NotificationSchema);

module.exports = Notification;
