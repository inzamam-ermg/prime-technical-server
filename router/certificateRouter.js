const express = require("express");
const CertificateTemplate = require("../models/CertificateTemplate");
const router = express.Router();

//add users
router.get("/", async (req, res) => {
  try {
    const documents = await CertificateTemplate.find();
    res.json(documents);
  } catch {
    res.json({ err: "not found" });
  }
});

//get certificate by id
router.get("/:id", async (req, res) => {
  try {
    const certificate = await CertificateTemplate.findOne({
      _id: req.params.id,
    });
    res.json(certificate);
  } catch (err) {
    res.json({ error: "certificate not found" });
  }
});

router.post("/certificateUpload", async (req, res) => {
  const newCertificate = new CertificateTemplate(req.body);
  const result = await newCertificate.save();
  if (result) {
    const documents = await CertificateTemplate.find();
    res.json(documents);
  }
});

router.post("/deleteCertificate", async (req, res) => {
  const ids = req.body;

  try {
    const result = await CertificateTemplate.deleteMany({ _id: { $in: ids } });
    if (result) {
      const documents = await CertificateTemplate.find();
      res.json(documents);
    }
  } catch {
    res.json({ err: "delete failed" });
  }
});

router.post("/deleteOneCertificate", async (req, res) => {
  const ids = req.body;
  // const objects = [];
  // ids.map((id) => {
  //   objects.push(ObjectId(id));
  // });

  try {
    const result = await CertificateTemplate.deleteMany({ _id: { $in: ids } });
    if (result) {
      res.send(true);
    }
  } catch {
    res.send({ err: "delete failed" });
  }
});

module.exports = router;
