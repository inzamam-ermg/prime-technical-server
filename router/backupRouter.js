// external imports
const express = require('express')
const fs = require('fs')
const fsExtra = require('fs-extra')
const { zip } = require('zip-a-folder')
const schedule = require('node-schedule')
const crypto = require('crypto')
const { randomBytes } = require('crypto')

//internal import
const createExcelSheet = require('../utils/CreateExcelSheet')
const sendEmail = require('../utils/sendEmail')
const User = require('../models/People')
const Job = require('../models/Job')
const logs = require('../models/Log')
const StickerTemplate = require('../models/stickerTemplate')
const CertificateTemplate = require('../models/CertificateTemplate')
const CheckList = require('../models/CheckList')
const picturesByUrl = require('../utils/picturesByUrl')
const certificatesByUrl = require('../utils/certificatesByUrl')
const downloadFileByUrl = require('../utils/downloadFileByUrl')
const router = express.Router()
const fetch = require('cross-fetch')
const { uploadFile } = require('../utils/s3Bucket')
router.get('/', async (req, res) => {
  try {
    if (!fs.existsSync('./Backup')) {
      fs.mkdirSync('./Backup')
    }
    await handleDownloadAll()
    fs.readFile(`./backup.zip`, (err, data) => {
      if (err) throw err
      res.send(data.toString('base64'))
      fs.unlinkSync(`./backup.zip`)
      fsExtra.emptyDirSync('./Backup')
    })
  } catch (error) {
    console.log(error)
  }
})
router.post('/schedule', async (req, res) => {
  const email = req.body.email
  try {
    if (!fs.existsSync('./Backup')) {
      fs.mkdirSync('./Backup')
    }
  } catch (err) {
    console.error(err)
  }
  const cornExpression = req.body.schedule
  const type = req.body.type

  try {
    let current_job = schedule.scheduledJobs[type] 
    if (current_job) {
      current_job.cancel()
    }
    res.send(type)
   
    const job = schedule.scheduleJob(type,cornExpression, async () => {  
      if (type === 'all') {
        await handleDownloadAll()
      }  else if (type === 'jobs') {
        await handleDownloadJob(from, to, users)
      } else if (type === 'checklists') {
        await handleDownloadChecklist(from, to)
      } else if (type === 'certificates') {
        await handleDownloadCertificateTemplates(from, to)
      } else if (type === 'users') {
        await handleDownloadUsers(from, to)
      } else if (type === 'logs') {
        await handleDownloadLogFiles(from, to)
      } else if (type === 'stickers') {
        await handleDownloadStickerTemplate(from, to)
      }
      
      const url = await handleS3Url()
      await SchedulerBackUpByMail(email, url)
    })
    console.log('job start', email)
  } catch (err) {
    console.log(err)
    res.json({ err: 'job is not started for unknown error' })
  }
})

router.post('/manualBackUp', async (req, res) => {
  try {
    if (!fs.existsSync('./Backup')) {
      fs.mkdirSync('./Backup')
    }
  } catch (err) {
    console.error(err)
  }
  const to = req.body.to
  const from = req.body.from
  const users = req.body.users
  const type = req.body.type
  try {
    if (type === 'all') {
      await handleDownloadLogFiles(from, to)
      await handleDownloadCertificateTemplates(from, to)
      await handleDownloadStickerTemplate(from, to)
      await handleDownloadChecklist(from, to)
      await handleDownloadUsers(from, to)
      await handleDownloadJob(from, to, users)
    } else if (type === 'jobs') {
      await handleDownloadJob(from, to, users)
    } else if (type === 'checklists') {
      await handleDownloadChecklist(from, to)
    } else if (type === 'certificates') {
      await handleDownloadCertificateTemplates(from, to)
    } else if (type === 'users') {
      await handleDownloadUsers(from, to)
    } else if (type === 'logs') {
      await handleDownloadLogFiles(from, to)
    } else if (type === 'stickers') {
      await handleDownloadStickerTemplate(from, to)
    }

    zip('./Backup', 'backup.zip').then(() => {
      fs.readFile('./backup.zip', (err, data) => {
        if (err) throw err
        res.send(data.toString('base64'))
        fs.unlinkSync('./backup.zip')
        fsExtra.emptyDirSync('./Backup')
      })
    })
  } catch (err) {
    res.json({ err: 'Unknown error' })
  }
})
router.post('/singleJob/:jobNo', async (req, res) => {
  try {
    if (!fs.existsSync('./Backup')) {
      fs.mkdirSync('./Backup')
    }

    const userData = handleName(req.body)

    const documents = await Job.find({ jobNo: req.params.jobNo })
    const job = documents[0]
    const zipFileName = job.jobNo
    const tempObj = { ...job }
    const jobArray = Object.entries(tempObj._doc)
    createExcelSheet('jobDetails', jobArray, 100)
    const checkLists = job.checklists
    const quotations = job.quotations
    const invoices = job.invoices

    await handleCertificateFolder(checkLists, userData)
    if (quotations.length) {
      await handleQuotationsAndInvoices(quotations, 'Quotations')
    }
    if (invoices.length) {
      await handleQuotationsAndInvoices(quotations, 'invoices')
    }

    zip('./Backup', `${zipFileName}.zip`).then(() => {
      fs.readFile(`./${zipFileName}.zip`, (err, data) => {
        if (err) throw err
        res.send(data.toString('base64'))
        fs.unlinkSync(`./${zipFileName}.zip`)
        fsExtra.emptyDirSync('./Backup')
      })
    })
  } catch (err) {
    res.json({ err: 'Unknown error' })
  }
})

router.post('/allJob', async (req, res) => {
  try {
    if (!fs.existsSync('./Backup')) {
      fs.mkdirSync('./Backup')
    }

    const userData = handleName(req.body)

    const documents = await Job.find({ isComplete: true })

    for (let index = 0; index < documents.length; index++) {
      const singleJob = documents[index]
      const jobFolderName = singleJob.jobNo
      const tempObj = { ...singleJob }
      const pathName = `./Backup/${jobFolderName}`
      fs.mkdirSync(pathName)
      const jobArray = Object.entries(tempObj._doc)
      createExcelSheet('jobDetails', jobArray, 100, jobFolderName)
      const checkLists = singleJob.checklists
      const quotations = singleJob.quotations
      const invoices = singleJob.invoices
      if (checkLists.length) {
        await handleCertificateFolder(checkLists, userData, jobFolderName)
      }
      if (quotations.length) {
        await handleQuotationsAndInvoices(
          quotations,
          'Quotations',
          jobFolderName,
        )
      }
      if (invoices.length) {
        await handleQuotationsAndInvoices(quotations, 'invoices', jobFolderName)
      }

      if (index === documents.length - 1) {
        zip('./Backup', `AllJobBackup.zip`).then(() => {
          fs.readFile(`./AllJobBackup.zip`, (err, data) => {
            if (err) throw err
            res.send(data.toString('base64'))
            fs.unlinkSync(`./AllJobBackup.zip`)
            fsExtra.emptyDirSync('./Backup')
          })
        })
      }
    }
  } catch (err) {
    console.log(err)
    res.json({ err: 'Unknown error' })
  }
})
router.post('/BackUpSendByEmail', async (req, res) => {
  try {
    const subject = req.body.subject
    const body = req.body.url
    const email = req.body.email
    const result = await sendEmail(email, subject, body)
    console.log('Email is sent', result.accepted)
    if (result?.accepted?.length) {
      res.send(true)
    } else {
      res.send(false)
    }
  } catch (error) {
    console.log(error)
    res.json({ err: "couldn't send" })
  }
})
const SchedulerBackUpByMail = async (email, url) => {
  try {
    const subject = 'Back up TPL Data'
    const body = `All BackUp data here at ${url} `
    const result = await sendEmail(email, subject, body)
    console.log('Email is sent', result.accepted)
    fs.unlinkSync('./backup.zip')
    fsExtra.emptyDirSync('./Backup')
    if (result.accepted.length) {
      return true
    } else {
      return false
    }
  } catch (error) {
    console.log(error)
  }
}
const handleCertificateFolder = async (checkLists, userData, jobFolderName) => {
  console.log(userData)
  for (let index = 0; index < checkLists.length; index++) {
    const ch = checkLists[index]
    ch.salesMan = handleNameById(userData, ch.salesMan)
    ch.inspector = handleNameById(userData, ch.inspector)
    const objArray = Object.entries(ch)
    const name = ch.certificateNo
    let pathName
    let sheetFolderName
    if (jobFolderName) {
      pathName = `./Backup/${jobFolderName}/${name}`
      sheetFolderName = `${jobFolderName}/${name}`
    } else {
      pathName = `./Backup/${name}`
      sheetFolderName = name
    }
    fs.mkdirSync(pathName)
    await createExcelSheet(name, objArray, 100, sheetFolderName)
    if (ch.pictures) {
      const extension = '.png'
      await picturesByUrl(ch.pictures, sheetFolderName, extension)
    }
    await downloadFileByUrl(
      ch.certificateUrl,
      `certificate.pdf`,
      sheetFolderName,
    )
    if (index === checkLists.length - 1) {
      return true
    }
  }
}
const handleQuotationsAndInvoices = async (array, name, jobFolderName) => {
  let pathName
  let sheetFolderName
  if (jobFolderName) {
    pathName = `./Backup/${jobFolderName}/${name}`
    sheetFolderName = `${jobFolderName}/${name}`
  } else {
    pathName = `./Backup/${name}`
    sheetFolderName = name
  }
  fs.mkdirSync(pathName)
  for (let index = 0; index < array.length; index++) {
    const element = array[index]
    await downloadFileByUrl(element.value, element.name, sheetFolderName)
    if (index === array.length - 1) {
      return true
    }
  }
}
const handleUserData = async () => {
  const doc = await User.find({})

  const userData = []
  for (let index = 0; index < doc.length; index++) {
    const user = doc[index]
    const userObj = JSON.stringify(user)
    const myObj = JSON.parse(userObj)
    const id = myObj._id
    const name = myObj.name
    const data = { id, name }
    userData.push(data)
  }
  return userData
}

const handleName = (array) => {
  const users = []
  array.map((item) => {
    const data = { name: item.name, id: item._id }
    users.push(data)
  })

  return users
}
const handleNameById = (array, value) => {
  return array.find((item) => item.id === value).name
}
const handleDownloadLogFiles = async (from, to) => {
  try {
    const folderName = 'Logs'
    let pathName = `./Backup/${folderName}`
    fs.mkdirSync(pathName)
    let documents
    if (from && to) {
      documents = await logs.find({
        createdAt: {
          $gte: new Date(from).toISOString(),
          $lt: new Date(to).toISOString(),
        },
      })
    } else {
      documents = await logs.find({})
    }

    const data = []
    if (documents.length) {
      documents.map((item) => {
        const obj = { ...item }
        const myObj = obj._doc
        delete myObj._id
        delete myObj.location
        delete myObj.__v
        data.push(myObj)
      })
    }

    if (data.length) {
      await createExcelSheet('Log_BackUp', data, 100, folderName)
    }
    return true
  } catch (error) {
    console.log(error)
  }
}
const handleDownloadChecklist = async (from, to) => {
  try {
    let documents
    if (from && to) {
      documents = await CheckList.find({
        createdAt: {
          $gte: new Date(from).toISOString(),
          $lt: new Date(to).toISOString(),
        },
      })
    } else {
      documents = await CheckList.find({})
    }
    if (documents.length) {
      const folderName = 'Checklists'
      let pathName = `./Backup/${folderName}`
      fs.mkdirSync(pathName)
      for (let index = 0; index < documents.length; index++) {
        const item = documents[index]
        const name = item.name
        const checklist = item.checkList
        await createExcelSheet(name, checklist, 200, folderName)
      }
    }

    return true
  } catch (error) {
    console.log(error)
  }
}
const handleDownloadCertificateTemplates = async (from, to) => {
  try {
    let documents
    if (from && to) {
      documents = await CertificateTemplate.find({
        createdAt: {
          $gte: new Date(from).toISOString(),
          $lt: new Date(to).toISOString(),
        },
      })
    } else {
      documents = await CertificateTemplate.find({})
    }
    const data = []
    if (documents.length) {
      documents.map((item) => {
        const obj = { name: item.name, value: item.value }
        data.push(obj)
      })
    }

    if (data.length) {
      const folderName = 'CertificateTemplates'
      pathName = `./Backup/${folderName}`
      fs.mkdirSync(pathName)

      await certificatesByUrl(data, folderName)
    }
    return true
  } catch (error) {
    console.log(error)
  }
}
const handleDownloadStickerTemplate = async (from, to) => {
  try {
    let documents
    if (from && to) {
      documents = await StickerTemplate.find({
        createdAt: {
          $gte: new Date(from).toISOString(),
          $lt: new Date(to).toISOString(),
        },
      })
    } else {
      documents = await StickerTemplate.find({})
    }
    const data = []
    if (documents.length) {
      documents.map((item) => {
        const obj = { name: item.name, value: item.value }
        data.push(obj)
      })
    }

    if (data.length) {
      const folderName = 'StickerTemplates'
      pathName = `./Backup/${folderName}`
      fs.mkdirSync(pathName)

      await certificatesByUrl(data, folderName)
    }
    return true
  } catch (error) {
    console.log(error)
  }
}
const handleDownloadUsers = async (from, to) => {
  try {
    let documents
    if (from && to) {
      documents = await User.find({
        createdAt: {
          $gte: new Date(from).toISOString(),
          $lt: new Date(to).toISOString(),
        },
      })
    } else {
      documents = await User.find({})
    }

    const data = []
    if (documents.length) {
      const folderName = 'Users'
      let pathName = `./Backup/${folderName}`
      fs.mkdirSync(pathName)
      documents.map((item) => {
        const obj = { ...item }
        const myObj = obj._doc
        delete myObj._id
        delete myObj.__v
        data.push(myObj)
      })
      if (data.length) {
        await createExcelSheet('Users', data, 100, folderName)
      }
    }

    return true
  } catch (error) {
    console.log(error)
  }
}
const handleDownloadJob = async (from, to) => {
  try {
    let documents
    if (from && to) {
      documents = await Job.find({
        createdAt: {
          $gte: new Date(from).toISOString(),
          $lt: new Date(to).toISOString(),
        },
        isComplete: true,
      })
    } else {
      documents = await Job.find({ isComplete: true })
    }
    const userData = await handleUserData()
    console.log('users', userData)

    if (documents.length) {
      const folderName = 'Jobs'
      let pathName = `./Backup/${folderName}`
      fs.mkdirSync(pathName)
      for (let index = 0; index < documents.length; index++) {
        const singleJob = documents[index]
        const jobFolderName = singleJob.jobNo
        const tempObj = { ...singleJob }
        const pathName = `./Backup/${folderName}/${jobFolderName}`

        fs.mkdirSync(pathName)
        const jobFolder = `${folderName}/${jobFolderName}`
        const jobArray = Object.entries(tempObj._doc)
        createExcelSheet('jobDetails', jobArray, 100, jobFolder)
        const checkLists = singleJob.checklists
        const quotations = singleJob.quotations
        const invoices = singleJob.invoices
        if (checkLists.length) {
          await handleCertificateFolder(checkLists, userData, jobFolder)
        }
        if (quotations.length) {
          await handleQuotationsAndInvoices(quotations, 'Quotations', jobFolder)
        }
        if (invoices.length) {
          await handleQuotationsAndInvoices(quotations, 'invoices', jobFolder)
        }
      }
    }

    return true
  } catch (error) {
    console.log(error)
  }
}
const handleDownloadAll = async () => {
  await handleDownloadLogFiles()
  await handleDownloadCertificateTemplates()
  await handleDownloadStickerTemplate()
  await handleDownloadChecklist()
  await handleDownloadUsers()
  await handleDownloadJob()
  await zip('./Backup', 'backup.zip')
  return true
}

const handleS3Url = async () => {
  const rawBytes = await randomBytes(16)
  const imageName = rawBytes.toString('hex')
  const file = {
    path: './backup.zip',
    filename: imageName,
  }
  console.log(file)

  try {
    const result = await uploadFile(file)
    console.log(result)
    const key = result.Key
    const url = `${process.env.NEXT_PUBLIC_API_BASE_URL}/${key}`
    return url
  } catch (err) {
    console.log(err)
  }
}
module.exports = router
