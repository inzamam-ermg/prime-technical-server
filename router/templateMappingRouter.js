const express = require("express");
const CertificateTemplate = require("../models/CertificateTemplate");
const CheckList = require("../models/CheckList");
const CertificateMap = require("../models/CertificateMap");
const uploadExcel = require("../utils/uploadExcel");
const fileUpload = require("express-fileupload");

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const docs = await CertificateMap.find();
    res.json(docs);
  } catch {
    res.json({ error: "unknown error" });
  }
});

router.get("/checklistAndCertificates", async (req, res) => {
  let data = [];

  try {
    const checklistData = await CheckList.find();
    const CertificateData = await CertificateTemplate.find();

    data = [[...checklistData], [...CertificateData]];
    res.json(data);
  } catch {
    res.json({ error: "checklistAndCertificates no found" });
  }
});

router.post("/save", async (req, res) => {
  try {
    const newData = new CertificateMap(req.body);
    const result = await newData.save();

    if (result) {
      const doc = await CertificateMap.find();
      res.json(doc);
    }
  } catch (error) {
    res.json({ err: "unknown error" });
  }
});

router.delete("/delete/:id", async (req, res) => {
  const eleId = req.params.id;
  try {
    const result = await CertificateMap.deleteOne({ _id: eleId });
    if (result) {
      const docs = await CertificateMap.find();
      res.json(docs);
    }
  } catch (err) {
    res.json({ err: "could not delete" });
  }
});
router.post("/upload", fileUpload(), async (req, res) => {
  try {
    if (req.files === null) {
      return res.status(400).json({ msg: "no file uploaded" });
    } else {
      const file = req.files.file;
      const fileName = file.name;

      // res.send(true);
      uploadExcel(fileName, file, res);
    }
  } catch {
    res.json({ error: "file upload failed" });
  }
});
router.patch("/update/:id", async (req, res) => {
  console.log(req.params.id);
  const eleId = req.params.id;
  const data = req.body;

  try {
    const stats = await CertificateMap.updateOne(
      { _id: eleId },
      {
        linkedField: data,
      }
    );
    if (stats.modifiedCount > 0) {
      res.send(true);
    }
  } catch (err) {
    res.json({ error: err });
  }
});
//get mapped link by id
router.get("/:id", async (req, res) => {
  try {
    const map = await CertificateMap.findOne({
      _id: req.params.id,
    });
    res.json(map);
  } catch (err) {
    res.json({ error: "CertificateMap field not found" });
  }
});
module.exports = router;
