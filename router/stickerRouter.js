const express = require("express");
const StickerTemplate = require("../models/stickerTemplate");
const router = express.Router();

//add users
router.get("/", async (req, res) => {
  try {
    const documents = await StickerTemplate.find();
    res.json(documents);
  } catch {
    res.json({ err: "not found" });
  }
});

//get certificate by id
router.get("/:id", async (req, res) => {
  try {
    const certificate = await StickerTemplate.findOne({
      _id: req.params.id,
    });
    res.json(certificate);
  } catch (err) {
    res.json({ error: "sticker not found" });
  }
});

router.post("/upload", async (req, res) => {
  const newCertificate = new StickerTemplate(req.body);
  const result = await newCertificate.save();
  if (result) {
    const documents = await StickerTemplate.find();
    res.json(documents);
  }
});

router.post("/delete", async (req, res) => {
  const ids = req.body;

  try {
    const result = await StickerTemplate.deleteMany({ _id: { $in: ids } });
    if (result) {
      const documents = await StickerTemplate.find();
      res.json(documents);
    }
  } catch {
    res.json({ err: "delete failed" });
  }
});

router.post("/deleteOne", async (req, res) => {
  const ids = req.body;
  // const objects = [];
  // ids.map((id) => {
  //   objects.push(ObjectId(id));
  // });

  try {
    const result = await StickerTemplate.deleteMany({ _id: { $in: ids } });
    if (result) {
      res.send(true);
    }
  } catch {
    res.send({ err: "delete failed" });
  }
});

module.exports = router;
