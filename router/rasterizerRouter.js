const express = require("express");
const { exec } = require("child_process");
const fs = require("fs");
const fsExtra = require("fs-extra");
const path = require("path");
const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploadedPdf");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname); //Appending extension
  },
});

const upload = multer({ storage: storage }).single("pdf");

const router = express.Router();

function run(cmd) {
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) return reject(error);
      if (stderr) return reject(stderr);
      resolve(true);
    });
  });
}

// get user
router.post("/", async (req, res) => {
  try {
    const result = await run(
      `rm -r uploadedPdf rasterizedPdf; mkdir uploadedPdf rasterizedPdf`
    );
    result &&
      upload(req, res, async (err) => {
        if (err) {
          res.send("something went wrong");
        } else {
          // console.log(req.file);
          // inkscape ${req.file.path} --export-type="svg" --export-filename=rasterizedPdf/output.svg
          //pdf2ps ${req.file.path} rasterizedPdf/output.ps && ps2pdf rasterizedPdf/output.ps rasterizedPdf/${req.file.filename}
          // `pdf2svg ${req.file.path} rasterizedPdf/output%d.svg all && rsvg-convert -f pdf -o rasterizedPdf/${req.file.filename} rasterizedPdf/*.svg`
          // `pdf2svg ${req.file.path} rasterizedPdf/output%d.svg all && rsvg-convert -d 300 -p 300 -f pdf -o rasterizedPdf/${req.file.filename} rasterizedPdf/*.svg`;

          const cmdres = await run(
            `pdf2svg ${req.file.path} rasterizedPdf/output%d.svg all && rsvg-convert -d 300 -p 300 -f pdf -o rasterizedPdf/${req.file.filename} rasterizedPdf/*.svg`
          );
          if (cmdres) {
            console.log(cmdres);
            res.download(`rasterizedPdf/${req.file.filename}`);
            // fs.readFile(`./rasterizedPdf/${req.file.filename}`, (err, data) => {
            //   if (err) {
            //     res.send({ err: err });
            //   } else {
            //     res.send(data.toString("base64"));
            //   }
            // });
          }
        }
      });
    // res.send("I'm working");
  } catch (err) {
    res.json({ error: "can't upload" });
  }
});

module.exports = router;
