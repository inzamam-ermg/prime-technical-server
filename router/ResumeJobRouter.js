const express = require("express");
const resumeJob = require("../models/resumeJob");

const router = express.Router();

//get resumeJob
router.get("/", async (req, res) => {
  try {
    const docs = await resumeJob.find();
    res.json(docs);
  } catch {
    res.json({ error: "unknown error" });
  }
});
//get job by inspector id
router.get("/:id", async (req, res) => {
  try {
    const job = await resumeJob.find({ inspectors: req.params.id });
    res.json(job);
  } catch (err) {
    res.json({ error: "job not found" });
  }
});
//get EditedJob by inspector id
router.get("/edit/:id", async (req, res) => {
  try {
    const job = await resumeJob.find({
      AssignInspector: req.params.id,
      isEdit: true,
    });
    res.json(job);
  } catch (err) {
    res.json({ error: "job not found" });
  }
});

//add resumeJob
router.post("/", async (req, res) => {
  try {
    const newData = new resumeJob(req.body);
    const result = await newData.save();

    if (result) {
      const doc = await resumeJob.find();
      res.json(doc);
    }
  } catch (error) {
    res.json({ err: "unknown error" });
  }
});
//update checklists by id
router.patch("/:id", async (req, res) => {
  const userId = req.params.id;
  const data = req.body;

  try {
    const stats = await resumeJob.updateOne(
      { _id: userId },
      {
        checklists: data,
      }
    );

    res.json(stats);
  } catch (err) {
    res.json({ error: err });
  }
});
//update job status  by adding checklist and newChecklist

router.patch("/edit/:id", async (req, res) => {
  const userId = req.params.id;
  const checklists = req.body.checklists;
  const newChecklists = req.body.newChecklists;

  try {
    const stats = await resumeJob.updateOne(
      { _id: userId },
      {
        checklists: checklists,
        newChecklists: newChecklists,
      }
    );
    console.log(stats);

    res.json(stats);
  } catch (err) {
    res.json({ error: err });
  }
});

//update inspectors by id
router.patch("/inspector/:id", async (req, res) => {
  const userId = req.params.id;
  const data = req.body;

  try {
    const stats = await resumeJob.updateOne(
      { _id: userId },
      {
        inspectors: data,
      }
    );

    res.json(stats);
  } catch (err) {
    res.json({ error: err });
  }
});

//remove resume job by id

router.delete("/:id", async (req, res) => {
  try {
    const data = await resumeJob.findByIdAndDelete({
      _id: req.params.id,
    });
    res.send(data);
  } catch {
    res.send("Wrong parameter detected");
  }
});

module.exports = router;
