// external imports
const express = require("express");
const fileUpload = require("express-fileupload");
const fs = require("fs");
const util = require("util");
const multer = require("multer");
const upload = multer({ dest: "uploads/" });
const unlinkFile = util.promisify(fs.unlink);
const aws = require("aws-sdk/clients/s3");

const bucketName = process.env.BUCKET_NAME;
const accessKeyId = process.env.ACCESS_KEY;
const secretAccessKey = process.env.SECRET_KEY;
const region = process.env.REGION;

const s3 = new aws({
  accessKeyId,
  secretAccessKey,
  region,
  signatureVersion: "v4",
});

// internal imports
const {
  getFileStream,
  deleteFile,
  generateUploadUrl,
  uploadFile,
  uploadFileByKey,
  uploadSignature,
} = require("../utils/s3Bucket");
const router = express.Router();

router.post(
  "/upload",
  upload.single("myFile"),
  async function (req, res, next) {
    try {
      const result = await uploadFile(req.file);
      await unlinkFile(req.file.path);
      res.json(result.Key);
    } catch (err) {
      res.json({ err: "failed to upload " });
    }
  }
);
router.post(
  "/upload/:key",
  upload.single("myFile"),
  async function (req, res, next) {
    try {
      const key = req.params.key;
      const result = await uploadFileByKey(req.file, key);
      await unlinkFile(req.file.path);
      res.json(result);
    } catch (err) {
      res.json({ err: "failed to upload " });
    }
  }
);
router.get("/getUrl", async (req, res) => {
  try {
    const result = await generateUploadUrl();
    res.json(result);
  } catch (error) {
    res.json({ err: "failed to delete" });
  }
});
router.delete("/delete/:key", async (req, res) => {
  const key = req.params.key;
  try {
    const result = await deleteFile(key);
    if (result) {
      console.log(result)
      res.send(true);
    }
    else{
      res.send(false);

    }
  } catch (error) {
    res.json({ err: "failed to delete" });
  }
});

router.post("/uploadSignature", fileUpload(), async (req, res) => {
  try {
    const imageBuffer = req.files.file;
    console.log(imageBuffer);

    const link = await uploadSignature(imageBuffer);
    // upload this buffer on AWS S3
    console.log(link);
  } catch (error) {
    console.log(error);
    res.json({
      status: "error",
      data: error.message || "Something went wrong",
    });
    // return callback(error);
  }
});

module.exports = router;
