const express = require("express");
const fileUpload = require("express-fileupload");
const ExcelToJson = require("../utils/ExcelToJson");
const CheckList = require("../models/CheckList");

const router = express.Router();

//get checklist
router.get("/", async (req, res) => {
  try {
    const docs = await CheckList.find();
    res.json(docs);
  } catch {
    res.json({ error: "unknown error" });
  }
});

//add checklist
router.post("/checkListUpload", fileUpload(), async (req, res) => {
  try {
    if (req.files === null) {
      return res.status(400).json({ msg: "no file uploaded" });
    } else {
      const file = req.files.file;
      const fileName = file.name;

      // res.send(true);
      ExcelToJson(fileName, file, res);
    }
  } catch {
    res.json({ error: "file upload failed" });
  }
});

router.post("/deleteCheckList", async (req, res) => {
  const ids = req.body;
  const objects = [];
  // ids.map((id) => {
  //   objects.push(ObjectId(id));
  // });

  try {
    const result = await CheckList.deleteMany({ _id: { $in: ids } });
    if (result) {
      const documents = await CheckList.find();
      res.json(documents);
    }
  } catch {
    res.json({ err: "delete failed" });
  }
});

router.post("/deleteOneCheckList", async (req, res) => {
  const ids = req.body;
  const objects = [];
  // ids.map((id) => {
  //   objects.push(ObjectId(id));
  // });

  try {
    const result = await CheckList.deleteMany({ _id: { $in: ids } });
    if (result) {
      res.send(true);
    }
  } catch {
    res.json({ err: "delete failed" });
  }
});

router.patch("/updateCheckListStatus", async (req, res) => {
  const id = req.body.id;
  const status = req.body.status;

  try {
    const result = await CheckList.updateOne(
      { _id: id },
      { $set: { status: !status } }
    );

    if (result) {
      const documents = await CheckList.find();
      res.json(documents);
    }
  } catch (err) {
    res.json({ err: "update failed" });
  }
});
//get checklist by id

router.get("/:id", async (req, res) => {
  try {
    const checklist = await CheckList.findOne({
      _id: req.params.id,
    });
    res.json(checklist);
  } catch (err) {
    res.json({ error: "checklist not found" });
  }
});

module.exports = router;
