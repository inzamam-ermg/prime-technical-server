const express = require('express')
const Notification = require('../models/Notification')

const router = express.Router()

//get
router.get('/', async (req, res) => {
  try {
    const docs = await Notification.find()
    res.json(docs)
  } catch {
    res.json({ error: 'unknown error' })
  }
})

//add
router.post('/', async (req, res) => {
  try {
    const newNotification = new Notification(req.body)
    const ack = await newNotification.save()
    res.json(ack)
  } catch {
    res.json({ error: 'post failed' })
  }
})

router.patch('/:id', async (req, res) => {
  const seenStatus = req.body.seenStatus
  console.log(seenStatus)
  try {
    const result = await Notification.updateOne(
      { _id: req.params.id },
      { $set: { seenStatus: seenStatus } },
    )

    if (result) {
      const documents = await Notification.find()
      res.json(documents)
    }
  } catch (err) {
    res.json({ err: 'update failed' })
  }
})

router.delete('/:id', async (req, res) => {
  try {
    const notificationRes = await Notification.deleteOne({
      _id: req.params.id,
    })

    if (notificationRes) {
      const documents = await Notification.find()
      res.json(documents)
    }
  } catch (err) {
    res.json({ err: 'Delete failed' })
  }
})
//get   by id

router.get('/:id', async (req, res) => {
  try {
    const notificationRes = await Notification.findOne({
      _id: req.params.id,
    })
    res.json(notificationRes)
  } catch (err) {
    res.json({ error: ' not found' })
  }
})

module.exports = router
