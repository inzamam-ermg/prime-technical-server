const express = require("express");

// internal imports
const CompanyDetail = require("../models/CompanyDetails");
const ContactDetail = require("../models/ContactDetails");

const router = express.Router();

const characters = "ABC6DEFGHI12JKLMN89OPQ4RSTUV35WXYZ7";

function generateString(length) {
  let result = "";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
}

//add company and contact
router.post("/", async (req, res) => {
  console.log(req.body);
  const companyData = { ...req.body };
  const allCompanyData = await CompanyDetail.find({
    companyName: companyData.companyName,
    contactPerson: companyData.contactPerson,
    contactNo: companyData.contactNo,
    client: companyData.client,
    clientEmail: companyData.clientEmail,
  });
  const allContactData = await ContactDetail.find({
    companyName: companyData.companyName,
    contactPerson: companyData.contactPerson,
    contactNo: companyData.contactNo,
    client: companyData.client,
    clientEmail: companyData.clientEmail,
  });

  if (allCompanyData.length <= 0 && allContactData.length <= 0) {
    const newCompanyData = new CompanyDetail({
      ...companyData,
      companyId: generateString(3),
    });
    const newContactData = new ContactDetail({
      ...companyData,
      contactId: generateString(3),
    });
    try {
      const companyResult = await newCompanyData.save();
      const contactResult = await newContactData.save();
      res.json({ company: companyResult, contact: contactResult });
    } catch (err) {
      res.status(500).json({
        errors: {
          common: {
            msg: "Unknown error occured!",
          },
        },
      });
    }
  } else if (allCompanyData.length <= 0) {
    const newCompanyData = new CompanyDetail({
      ...companyData,
      companyId: generateString(3),
    });
    try {
      const result = await newCompanyData.save();
      res.json({ company: result, contact: allContactData[0] });
    } catch (err) {
      res.status(500).json({
        errors: {
          common: {
            msg: "Unknown error occured!",
          },
        },
      });
    }
  } else if (allContactData.length <= 0) {
    const newContactData = new ContactDetail({
      ...companyData,
      contactId: generateString(3),
    });
    try {
      const result = await newContactData.save();
      res.json({ company: allCompanyData[0], contact: result });
    } catch (err) {
      res.status(500).json({
        errors: {
          common: {
            msg: "Unknown error occured!",
          },
        },
      });
    }
  } else {
    res.json({ company: allCompanyData[0], contact: allContactData[0] });
  }
});

// get user
router.get("/", async (req, res) => {
  try {
    const allCompanyData = await CompanyDetail.find();
    const allContactData = await ContactDetail.find();
    res.json({
      company: [...allCompanyData],
      contact: [...allContactData],
    });
  } catch (err) {
    res.json({ error: "no user found" });
  }
});

module.exports = router;
