const express = require('express')
const Banner = require('../models/Banner')

const router = express.Router()

//get Footer
router.get('/', async (req, res) => {
  try {
    const docs = await Banner.find()
    res.json(docs)
  } catch {
    res.json({ error: 'unknown error' })
  }
})

//add Footer
router.post('/', async (req, res) => {
  try {
    const newFooterImage = new Banner(req.body)
    const ack = await newFooterImage.save()
    res.json(ack)
  } catch {
    res.json({ error: 'post failed' })
  }
})

router.patch('/:id', async (req, res) => {
  const value = req.body.value
console.log(value)
  try {
    const result = await Banner.updateOne(
      {  _id: req.params.id },
      { $set: { value: value } },
    )

    if (result) {
      const documents = await Banner.find()
      res.json(documents)
    }
  } catch (err) {
    res.json({ err: 'update failed' })
  }
})
router.delete('/:id', async (req, res) => {
  try {
    const footerRes = await Banner.deleteOne({
      _id: req.params.id,
    })

    if (footerRes) {
      const documents = await Banner.find()
      res.json(documents)
    }
  } catch (err) {
    res.json({ err: 'Delete failed' })
  }
})
//get Footer Image by id

router.get('/:id', async (req, res) => {
  try {
    const footerRes = await Banner.findOne({
      _id: req.params.id,
    })
    res.json(footerRes)
  } catch (err) {
    res.json({ error: 'Footer Image not found' })
  }
})

module.exports = router
