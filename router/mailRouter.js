// external imports
const express = require('express')
const fs = require('fs')
const fsExtra = require('fs-extra')
const { zip } = require('zip-a-folder')

//internal import
const downloadFileByUrl = require('../utils/downloadFileByUrl')
const sendEmail = require('../utils/sendEmail')
const logs = require('../models/Log')
const createExcelSheet = require('../utils/CreateExcelSheet')

const router = express.Router()

router.post('/', async (req, res) => {
  try {
    const subject = req.body.subject
    const body = req.body.body
    const email = req.body.email
    const links = req.body.fileLinkAndName
    console.log(email)

    const result = await sendEmail(email, subject, body)
    console.log('Email is sent', result.accepted)
    if (result?.accepted?.length) {
      res.send(true)
    } else {
      res.send(false)
    }
  } catch (error) {
    res.json({ err: "couldn't send" })
  }
})
router.post('/sendCertificates', async (req, res) => {
  try {
    const subject = req.body.subject
    const body = req.body.body
    const email = req.body.email
    const links = req.body.fileLinkAndName
    await handleDownloadFiles(links)
    await zip('./Backup', 'AllDocuments.zip')
    const file = './AllDocuments.zip'

    const result = await sendEmail(email, subject, body, file)
    console.log('Email is sent', result.accepted)
    fs.unlinkSync('./AllDocuments.zip')
    fsExtra.emptyDirSync('./Backup')
    if (result?.accepted?.length) {
      res.send(true)
    } else {
      res.send(false)
    }
  } catch (error) {
    res.json({ err: "couldn't send" })
  }
})

router.post('/sendLogs', async (req, res) => {
  try {
    if (!fs.existsSync('./Backup')) {
      fs.mkdirSync('./Backup')
    }
    console.log(req.body)
    await handleDownloadLogFiles()
    const sendLogs = await sendLogsByMail(req.body.email)

    res.send(sendLogs)
  } catch (error) {
    console.log(error)
  }
})
router.post('/sendNotification', async (req, res) => {
  try {
    if (!fs.existsSync('./Backup')) {
      fs.mkdirSync('./Backup')
    }

    const subject = req.body.subject
    const body = req.body.body
    const email = req.body.email
    const docs = req.body.checkLists
    await handleDownloadChecklist(docs)
    await zip('./Backup', 'AllChecklist.zip')
    const file = './AllChecklist.zip'
    const result = await sendEmail(email, subject, body, file)
    console.log('Email is sent', result.accepted)
    fs.unlinkSync('./AllChecklist.zip')
    fsExtra.emptyDirSync('./Backup')
    if (result?.accepted?.length) {
      res.send(true)
    } else {
      res.send(false)
    }
  } catch (error) {
    console.log(error)
  }
})

const handleDownloadChecklist = async (documents) => {
  try {
    for (let index = 0; index < documents.length; index++) {
      const ch = documents[index]
      const objArray = Object.entries(ch)
      const name = ch.certificateNo
      let pathName
      let sheetFolderName
      pathName = `./Backup/${name}`
      sheetFolderName = name
      fs.mkdirSync(pathName)
      await createExcelSheet(name, objArray, 100, sheetFolderName)

      if (index === documents.length - 1) {
        return true
      }
    }

    return true
  } catch (error) {
    console.log(error)
  }
}

const handleDownloadLogFiles = async () => {
  try {
    let documents
    documents = await logs.find({})

    const data = []
    if (documents.length) {
      documents.map((item) => {
        const obj = { ...item }
        const myObj = obj._doc
        delete myObj._id
        delete myObj.location
        delete myObj.__v
        data.push(myObj)
      })
    }

    if (data.length) {
      await createExcelSheet('Log_BackUp', data, data.length)
    }
    return true
  } catch (error) {
    console.log(error)
  }
}

const handleDownloadFiles = async (links) => {
  try {
    if (!fs.existsSync('./Backup')) {
      fs.mkdirSync('./Backup')
    }
    for (let index = 0; index < links.length; index++) {
      const element = links[index]
      const name = element.name
      const url = element.link

      await downloadFileByUrl(url, name)
      if (index === links.length - 1) {
        return true
      }
    }
  } catch (error) {
    console.log(error)
  }
}
const sendLogsByMail = async (email) => {
  const file = './Backup/Log_BackUp.xlsx'
  try {
    const subject = 'Logs of TPL'
    const body = 'All Log Data'
    const EmailAttachMents = file
    const result = await sendEmail(email, subject, body, EmailAttachMents)
    console.log('Email is sent', result.accepted)
    fsExtra.emptyDirSync('./Backup')
    if (result.accepted.length) {
      return true
    } else {
      return false
    }
  } catch (error) {
    console.log(error)
  }
}

module.exports = router
