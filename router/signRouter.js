const express = require("express");
const { exec } = require("child_process");
const fs = require("fs");
const fsExtra = require("fs-extra");
const path = require("path");
const SignPDF = require("../utils/SignPDF");
const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploadedPdf");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname); //Appending extension
  },
});

const upload = multer({ storage: storage }).single("pdf");

const router = express.Router();

function run(cmd) {
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) return reject(error);
      if (stderr) return reject(stderr);
      resolve(true);
    });
  });
}

// get user
router.post("/", async (req, res) => {
  try {
    const result = await run(
      `rm -r uploadedPdf signedPdf; mkdir uploadedPdf signedPdf`
    );
    result &&
      upload(req, res, async (err) => {
        if (err) {
          res.send("something went wrong");
        } else {
          const pdfBuffer = new SignPDF(
            path.resolve(`uploadedPdf/${req.file.filename}`),
            path.resolve("test_assets/keyStore.p12")
          );

          const signedDocs = await pdfBuffer.signPDF();
          const pdfName = `./signedPdf/${req.file.filename}`;

          fs.writeFileSync(pdfName, signedDocs);
          console.log(`New Signed PDF created called: ${pdfName}`);

          res.download(`signedPdf/${req.file.filename}`);
        }
      });
    // res.send("I'm working");
  } catch (err) {
    res.json({ error: "can't upload" });
  }
});

module.exports = router;
