const express = require("express");
const Job = require("../models/Job");

const router = express.Router();

//get AllJob
router.get("/", async (req, res) => {
  try {
    const docs = await Job.find();
    res.json(docs);
  } catch {
    res.json({ error: "unknown error" });
  }
});
router.post("/singleJob", async (req, res) => {
  const jobNo = req.body.name;
  const inspector = req.body.inspector;
  const checklistData = [];
  try {
    const docs = await Job.find({
      isInspectionComplete: true,
      AssignInspector: inspector,
      isEdit: false,
    });

    docs.map((item) => {
      if (item.jobNo === jobNo) {
        checklistData.push(item);
      }
    });

    res.json(checklistData[0]);
  } catch {
    res.json({ error: "unknown error" });
  }
});

router.post("/checklist", async (req, res) => {
  const checklistName = req.body.name;
  const checklistData = [];
  try {
    const docs = await Job.find({ isInspectionComplete: true });

    docs.map((item) => {
      item.checklists.map((ch) => {
        if (ch.orderNo === checklistName) {
          checklistData.push(ch);
        }
      });
    });

    res.json(checklistData[0]);
  } catch {
    res.json({ error: "unknown error" });
  }
});

//get assignJob
router.get("/assignJob", async (req, res) => {
  try {
    const docs = await Job.find({ isInspectionComplete: false });
    res.json(docs);
  } catch {
    res.json({ error: "unknown error" });
  }
});

//get complete Jobs
router.get("/completeJob", async (req, res) => {
  try {
    const docs = await Job.find({ isComplete: true });
    res.json(docs);
  } catch {
    res.json({ error: "unknown error" });
  }
});

//get inspection  Complete Job

router.get("/InspectionCompleteJob", async (req, res) => {
  try {
    const docs = await Job.find({ isInspectionComplete: true });
    res.json(docs);
  } catch {
    res.json({ error: "unknown error" });
  }
});

//get job by inspector id
router.get("/:id", async (req, res) => {
  try {
    const job = await Job.find({ inspectors: req.params.id });
    res.json(job);
  } catch (err) {
    res.json({ error: "job not found" });
  }
});

//get job by job id
router.get("/jobById/:id", async (req, res) => {
  try {
    const job = await Job.findOne({ _id: req.params.id });
    res.json(job);
  } catch (err) {
    res.json({ error: "job not found" });
  }
});

//get Almost Expire Certificates
router.post("/reminderCertificates", async (req, res) => {
  try {
    const docs = await Job.find({ isInspectionComplete: true });
    const certificates = [];
    docs.map((eachJob) => {
      eachJob.checklists.map((item) => {
        const from = new Date(req.body.from);
        const to = new Date(req.body.to);
        const myDate = new Date(item.dateOfNextInspection);
        if (from <= myDate && myDate <= to) {
          certificates.push(item);
        }
      });
    });
    res.json(certificates);
  } catch {
    res.json({ error: "unknown error" });
  }
});

//add Job
router.post("/", async (req, res) => {
  try {
    const newData = new Job(req.body);
    const result = await newData.save();

    if (result) {
      const doc = await Job.find({ isInspectionComplete: false });
      res.json(doc);
    }
  } catch (error) {
    res.json({ err: "unknown error" });
  }
});

//add from inspector
router.post("/byInspector", async (req, res) => {
  try {
    const newData = new Job(req.body);
    const result = await newData.save();

    if (result) {
      const doc = await Job.find({ inspectors: req.body.inspectors[0] });
      res.json(doc);
    }
  } catch (error) {
    res.json({ err: "unknown error" });
  }
});

//update job status by adding certificate in it

router.patch("/:id", async (req, res) => {
  const userId = req.params.id;
  const data = req.body;

  try {
    const stats = await Job.updateOne(
      { _id: userId },
      {
        checklists: data,
      }
    );

    res.json(stats);
  } catch (err) {
    res.json({ error: err });
  }
});

//update job details by id

router.patch("/jobDetails/:id", async (req, res) => {
  const userId = req.params.id;
  const body = req.body;

  try {
    const stats = await Job.updateOne(
      { _id: userId },
      {
        client: body.client,
        clientEmail: body.clientEmail,
        contactPerson: body.contactPerson,
        contactNo: body.contactNo,
        whatsAppNo: body.whatsAppNo,
      }
    );

    if(stats){
      const job = await Job.findOne({ _id: req.params.id });
      res.json(job);

    }
  } catch (err) {
    res.json({ error: err });
  }
});

//update inspectors by id
router.patch("/inspector/:id", async (req, res) => {
  const userId = req.params.id;
  const data = req.body;

  try {
    const stats = await Job.updateOne(
      { _id: userId },
      {
        inspectors: data,
      }
    );

    res.json(stats);
  } catch (err) {
    res.json({ error: err });
  }
});
router.patch("/InspectionComplete/:id", async (req, res) => {
  const userId = req.params.id;
  try {
    const stats = await Job.updateOne(
      { _id: userId },
      {
        isInspectionComplete: true,
      }
    );
    res.json(stats);
  } catch (err) {
    res.json({ error: err });
  }
});
router.patch("/editComplete/:id", async (req, res) => {
  const userId = req.params.id;
  try {
    const stats = await Job.updateOne(
      { _id: userId },
      {
        isInspectionComplete: true,
        isEdit: true,
        isAuthorized: false,
      }
    );
    res.json(stats);
  } catch (err) {
    res.json({ error: err });
  }
});
router.patch("/authorization/:id", async (req, res) => {
  const userId = req.params.id;
  try {
    const stats = await Job.updateOne(
      { _id: userId },
      {
        isAuthorized: true,
        isEdit: false,
      }
    );
    res.json(stats);
  } catch (err) {
    res.json({ error: err });
  }
});

//update quotation

router.patch("/quotationUpload/:id", async (req, res) => {
  const userId = req.params.id;
  const data = req.body;

  try {
    const stats = await Job.updateOne(
      { _id: userId },
      {
        quotations: data,
      }
    );
    console.log(stats);

    const doc = await Job.find({ _id: userId });
    res.json(doc);
  } catch (err) {
    res.json({ error: err });
  }
});
//update Invoices

router.patch("/invoiceUpload/:id", async (req, res) => {
  const userId = req.params.id;
  const data = req.body;

  try {
    const stats = await Job.updateOne(
      { _id: userId },
      {
        invoices: data,
      }
    );
    console.log(stats);

    const doc = await Job.find({ _id: userId });
    res.json(doc);
  } catch (err) {
    res.json({ error: err });
  }
});

// update job status after complete by admin
router.patch("/complete/:id", async (req, res) => {
  const userId = req.params.id;
  try {
    const stats = await Job.updateOne(
      { _id: userId },
      {
        isComplete: true,
      }
    );
    res.json(stats);
  } catch (err) {
    res.json({ error: err });
  }
});
// update job status after marked as  inComplete by admin
router.patch("/inComplete/:id", async (req, res) => {
  const userId = req.params.id;
  try {
    const stats = await Job.updateOne(
      { _id: userId },
      {
        isComplete: false,
      }
    );
    res.json(stats);
  } catch (err) {
    res.json({ error: err });
  }
});

module.exports = router;
