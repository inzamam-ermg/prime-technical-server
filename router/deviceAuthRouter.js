const express = require("express");
const AuthRequest = require("../models/AuthRequest");
const User = require("../models/People");

const router = express.Router();

router.post("/", async (req, res) => {
  const newRequest = new AuthRequest(req.body);
  const data = await newRequest.save();
  res.json(data); //TODO: have to check data.ops[0] is working or not
});

router.get("/", async (req, res) => {
  try {
    const data = await AuthRequest.find();

    res.json(data);
  } catch {
    res.json({ err: "data not found" });
  }
});
router.get("/:email", async (req, res) => {
  try {
    const data = await AuthRequest.findOne({
      userEmail: req.params.email,
    });

    res.json(data);
  } catch {
    res.json({ err: "data not found" });
  }
});

router.get("/:deviceId", async (req, res) => {
  try {
    const data = await AuthRequest.findOne({
      _id: req.params.deviceId,
    });
    res.json(data);
  } catch {
    res.json({ error: "device not found" });
  }
});

router.patch("/:deviceId", async (req, res) => {
  const deviceId = req.params.deviceId;
  const data = req.body;
  const device = data.device;

  try {
    if (device.windows) {
      const userStatus = await User.updateOne(
        { email: req.body.userEmail },
        {
          $set: { "authorized.windows": device.windows },
        }
      );
    } else if (device.mobile) {
      const userStatus = await User.updateOne(
        { email: req.body.userEmail },
        {
          $set: { "authorized.mobile": device.mobile },
        }
      );
    } else {
      res.send("empty device data");
    }
    try {
      const result = await AuthRequest.findByIdAndDelete({
        _id: data._id,
      });
      res.send(result);
    } catch {
      res.send("Wrong parameter detected");
    }
  } catch (err) {
    res.json({ error: err });
  }
});

router.delete("/:id", async (req, res) => {
  const userId = req.params.id;
  try {
    const data = await AuthRequest.findByIdAndDelete({
      _id: userId,
    });
    res.send(data);
  } catch {
    res.send("Wrong parameter detected");
  }
});

module.exports = router;
